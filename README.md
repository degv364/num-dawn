# num-dawn
[![pipeline status](https://gitlab.com/degv364/num-dawn/badges/master/pipeline.svg)](https://gitlab.com/degv364/num-dawn/commits/master)

This is a project for building the numbers from their formal mathematical definition.
It is not intended for numerical computations, but for fun. The idea is to have an implementation
of the numbers from their definition so that one can create programs that reason about
numbers instead of just using them for calculating stuff.

## Installation

You will need [git](https://git-scm.com/) and [stack](https://docs.haskellstack.org/en/stable/README/)
installed.

### Arch Linux

```bash
sudo pacman -S git stack
```

### Debian

```bash
sudo apt-get install git stack
```

## Building the Package

Go to the root directory of this project and:

```bash
stack build
```