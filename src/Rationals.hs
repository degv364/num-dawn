-- Copyright (c) 2019 Daniel Garcia-Vaglio
-- Author: Daniel Garcia-Vaglio <degv364@gmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

module Rationals where
import qualified Naturals as N
import qualified Integers as Z
import Prelude hiding (div)
import qualified Data.Ratio as Ratio (numerator, denominator)

-- -----------------------------------------------------------------------------
-- Basic Definition
-- -----------------------------------------------------------------------------

-- Define the Rationals as a pair, of a integer and a natural
type Rat = (Z.Int, N.Nat)

-- Basic examples
q0 = ((N.Zero, N.Zero), N.Suc N.Zero)

q1 = ((N.Suc N.Zero, N.Zero), N.Suc N.Zero)
q_1 = ((N.Zero, N.Suc N.Zero), N.Suc N.Zero)

q1_2 = ((N.Suc N.Zero, N.Zero), (N.Suc . N.Suc) N.Zero)
q_1_2 = ((N.Zero, N.Suc N.Zero), (N.Suc . N.Suc) N.Zero)


-- -----------------------------------------------------------------------------
-- Operations in Rational numbers
-- -----------------------------------------------------------------------------

-- Sign functions
positive q = Z.positive $ fst q
negative q = Z.negative $ fst q

-- Define canonical form
canonical :: Rat -> Rat
canonical (z, N.Suc n) = can (Z.canonical z, N.Suc n)
  where can ((a, N.Zero), m) = ((N.gcpf a m, N.Zero),
                                 N.gcpf m a)
        can ((N.Zero, a), m) = ((N.Zero, N.gcpf a m),
                                 N.gcpf m a)

-- Define invertion
additiveInvert (z, n) = canonical (Z.invert z, n)

multiplicativeInvert (z, n) = canonical $ mi (Z.canonical z, n)
  where mi ((a, N.Zero), n) = ((n, N.Zero), a)
        mi ((N.Zero, a), n) = ((N.Zero, n), a)

-- Define equivalence
equivalence :: Rat -> Rat -> Bool
equivalence a b = canonical a == canonical b

-- Define order
less a b = lessq (canonical a) (canonical b)
  where mulzn (p, q) n = (N.mul p n, N.mul q n)
        lessq (x, m) (y, n) = Z.less (mulzn x n) (mulzn y m)

-- Define addition
add a b = canonical (addq (canonical a) (canonical b))
  where mulzn (p,q) n = (N.mul p n, N.mul q n)
        addq (x, m) (y, n) = (Z.add (mulzn x n) (mulzn y m), N.mul n m)

-- Define substraction
sub a b = add a (additiveInvert b)

-- Define multiplication
mul a b = canonical (mulq a b)
  where mulq (p, m) (q, n)= (Z.mul p q, N.mul m n)

-- Define division
div a b = mul a (multiplicativeInvert b)

-- Define Natural power
pow q N.Zero = ((N.Suc N.Zero, N.Zero), N.Suc N.Zero)
pow q (N.Suc n) = mul q (pow q n)

-- -----------------------------------------------------------------------------
-- Transformations between Types
-- -----------------------------------------------------------------------------

readable (z, n) = Z.readable z / N.readable n

define q = (Z.define (Ratio.numerator q), N.define (Ratio.denominator q))
