-- Copyright (c) 2019 Daniel Garcia-Vaglio
-- Author: Daniel Garcia-Vaglio <degv364@gmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.


module Naturals where
import Prelude hiding (div, mod, quot, gcd, lcd)
-- -----------------------------------------------------------------------------
-- Basic Definition
-- -----------------------------------------------------------------------------

-- Define the natural numbers using the Peano axioms
data Nat = Zero | Suc Nat deriving (Eq, Show) 

-- Basic examples
n0 = Zero
n1 = Suc n0
n2 = Suc n1
n3 = Suc n2
n4 = Suc n3
n5 = Suc n4
n6 = Suc n5
n7 = Suc n6
n8 = Suc n7
n9 = Suc n8


-- -----------------------------------------------------------------------------
-- Operations in Natural numbers
-- -----------------------------------------------------------------------------

-- Define succesor as function, not constructor
suc = Suc

-- Define addition
add n Zero = n
add n (Suc m) = add (Suc n) m

-- Define substraction
sub n Zero = n
sub Zero n = error "Unable to substract a Natural from Zero"
sub (Suc n) (Suc m) = sub n m

-- Define multiplication
mul n Zero = Zero
mul n (Suc m) = add n (mul n m)

-- Define order
less Zero Zero = False
less Zero n = True
less n Zero = False
less (Suc n) (Suc m) = less n m

-- Define module
mod _ Zero = error "Cannot use Zero as modulus"
mod Zero m = Zero
mod n m = if less n m
          then n
          else mod (sub n m) m

-- Define division
div m Zero = error "Cannot use Zero as divisor"
div m d = quot Zero (sub m (mod m d)) d
  where quot q Zero d = q
        quot q m d = quot (Suc q) (sub m d) d

-- Define the GCD: greatest common divisor
gcd m Zero = m
gcd Zero n = n
gcd m n = if less m n
          then gcd m (sub n m)
          else gcd n (sub m n)

-- Define the GCPF: greatest coprime factor
gcpf m n = div m (gcd m n)

-- Define the LCM: least common multiple
lcm m n = div (mul m n) (gcd m n)

-- Define the power
pow m Zero = Suc Zero
pow m (Suc n) = mul m (pow m n)

-- Define factorial
factorial Zero = Suc Zero
factorial (Suc n) = mul (Suc n) (factorial n)


-- -----------------------------------------------------------------------------
-- Some interesting sequences
-- -----------------------------------------------------------------------------

-- Define the sequence of all natural numbers
naturals = Zero : [Suc n | n <- naturals]

-- Define the sequence of all natural numbers greater than m
greaterNaturals m = filter (less m) naturals

-- Define the sequence of all even naturals
evenNat = filter (\ m -> mod m n2 == n0) naturals

-- Define the sequence of all prime naturals
primes = filterPrime (greaterNaturals n1)
  where filterPrime (p:ns) = p : filterPrime [n | n <-ns , mod n p /= Zero]

-- -----------------------------------------------------------------------------
-- Transformations between Types
-- -----------------------------------------------------------------------------

-- Transform naturals from a recursive application of Suc to a readable number
readable n = intFromNat n 0
  where intFromNat Zero z = z
        intFromNat (Suc n) z = intFromNat n (z+1)

-- Transform a integer value back to its axiomatic definition
define :: Integer -> Nat
define z = if z < 0
           then error "Unable to convert negative numbers to Naturals"
           else natFromInt z Zero
  where natFromInt 0 n = n
        natFromInt z n = natFromInt (z-1) (Suc n)
