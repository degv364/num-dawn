-- Copyright (c) 2019 Daniel Garcia-Vaglio
-- Author: Daniel Garcia-Vaglio <degv364@gmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.


module Integers where
import Prelude hiding (Int, div, quot)
import qualified Naturals as N
-- -----------------------------------------------------------------------------
-- Basic Definition
-- -----------------------------------------------------------------------------

-- Define the integer numbers using the Naturals
type Int = (N.Nat, N.Nat)

-- Basic examples
z0 = (N.Zero, N.Zero)

z1 = (N.Suc N.Zero, N.Zero)
z2 = ((N.Suc . N.Suc) N.Zero, N.Zero)
z3 = ((N.Suc . N.Suc . N.Suc) N.Zero, N.Zero)
z4 = ((N.Suc . N.Suc . N.Suc . N.Suc) N.Zero, N.Zero)
z5 = ((N.Suc . N.Suc . N.Suc . N.Suc . N.Suc) N.Zero, N.Zero)
z6 = ((N.Suc . N.Suc . N.Suc . N.Suc . N.Suc . N.Suc) N.Zero, N.Zero)

z_1 = (N.Zero, N.Suc N.Zero)
z_2 = (N.Zero, (N.Suc . N.Suc) N.Zero)
z_3 = (N.Zero, (N.Suc . N.Suc . N.Suc) N.Zero)
z_4 = (N.Zero, (N.Suc . N.Suc . N.Suc . N.Suc) N.Zero)
z_5 = (N.Zero, (N.Suc . N.Suc . N.Suc . N.Suc . N.Suc) N.Zero)
z_6 = (N.Zero, (N.Suc . N.Suc . N.Suc . N.Suc . N.Suc . N.Suc) N.Zero)

-- -----------------------------------------------------------------------------
-- Operations in Integer numbers
-- -----------------------------------------------------------------------------

-- Sign functions
positive z = N.less (snd z) (fst z)
negative = uncurry N.less

-- Define invertion
invert (n,m) = canonical (m, n)

-- Define Absolute value
absolute z = if positive z
             then z
             else invert z

-- Find the canonical form
canonical :: Int -> Int
canonical (n, N.Zero) = (n, N.Zero)
canonical (N.Zero, n) = (N.Zero, n)
canonical (N.Suc n, N.Suc m) = canonical (n, m)

-- Define Equivalence
equivalence a b = canonical a == canonical b

-- Define addition
add a b = canonical (N.add (fst a) (fst b), N.add (snd a) (snd b))

-- Define substraction
sub a b = add a (invert b)

-- Define order
less a b = N.less (fstc a) (fstc b) || N.less (sndc b) (sndc a)
  where fstc = fst . canonical
        sndc = snd . canonical

-- Define Multiplication
mul :: Int -> Int -> Int
mul a b = prod (canonical a) (canonical b)
  where prod (m, N.Zero) (n, N.Zero) = (N.mul m n, N.Zero)
        prod (m, N.Zero) (N.Zero, n) = (N.Zero, N.mul m n)
        prod (N.Zero, m) (n, N.Zero) = (N.Zero, N.mul m n)
        prod (N.Zero, m) (N.Zero, n) = (N.mul m n, N.Zero)

-- Define Natural Power
pow z N.Zero = (N.Suc N.Zero, N.Zero)
pow z (N.Suc a) = mul z (pow z a)

-- Define integer division
div a b = quot (canonical a) (canonical b)
  where quot (m, N.Zero) (n, N.Zero) = (N.div m n, N.Zero)
        quot (m, N.Zero) (N.Zero, n) = (N.Zero, N.div m n)
        quot (N.Zero, m) (n, N.Zero) = (N.Zero, N.div m n)
        quot (N.Zero, m) (N.Zero, n) = (N.div m n, N.Zero)


-- -----------------------------------------------------------------------------
-- Transformations between Types
-- -----------------------------------------------------------------------------

-- Transforms Integers from Natural pairs to Integral types
readable (n,m) = N.readable n - N.readable m

-- Transform Prelude.Integer to Natural pairs
define z = if z < 0
           then (N.Zero, N.define (-z))
           else (N.define z, N.Zero)


intFromNat n = (n, N.Zero)
natFromInt z = fst (canonical z)
