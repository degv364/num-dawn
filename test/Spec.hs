-- Copyright (c) 2019 Daniel Garcia-Vaglio
-- Author: Daniel Garcia-Vaglio <degv364@gmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

import qualified Naturals as N
import qualified Integers as Z
import Test.Hspec
import Test.QuickCheck
import Control.Monad
import Control.Exception (evaluate)

-- Create an arbitrary Natural
instance Arbitrary N.Nat where
  arbitrary = oneof (map return (take 100 N.naturals))


main :: IO ()
main = hspec $ do
  describe "num-dawn.Naturals" $ do
    it "1+1 = 2" $
      N.add (N.Suc N.Zero) (N.Suc N.Zero) `shouldBe` (N.Suc . N.Suc) N.Zero

    it "The succesor is bigger" $
      property $ \n -> N.less n (N.Suc n)

    it "The succesor is the same as adding 1" $
      property $ \n -> N.suc n == N.add n N.n1

    it "Addition is commutative" $
      property $ \m n -> N.add m n == N.add n m

    it "Addition is associative" $
      property $ \m n p -> N.add (N.add m n) p == N.add m (N.add n p)

    it "Multiplication is commutative" $
      property $ \m n -> N.mul m n == N.mul n m

    it "Multiplication is associative" $
      -- Using only 3 test cases because 3 multiplications are quite heavy
      withMaxSuccess 3 (\m n p-> N.mul (N.mul m n) p == N.mul m (N.mul n p))

    it "Distribution of addition over product" $
      property $ \m n p -> N.mul m (N.add n p) == N.add (N.mul m n) (N.mul m p)

    it "Convert from Prelude Ints" $
      N.define 3 `shouldBe` N.n3

    it "Convert to Prelude Ints" $
      N.readable N.n3 `shouldBe` 3

    it "Conversion to and from prelude" $
      property $ \n -> (N.define . N.readable) n == n

    it "Conversion from and to prelude" $
      property $ \n -> (N.readable . N.define) (abs n) == abs n

    it "Substracting to get negatives fails" $
      evaluate (N.sub N.n3 N.n4) `shouldThrow` anyException

    it "Division by Zero fails" $
      evaluate (N.div N.n1 N.n0) `shouldThrow` anyException

  describe "num-dawn.Integers" $ do
    it "1+1 = 2" $
      Z.add Z.z1 Z.z1 `shouldBe` Z.z2

    it "Convert from positive Prelude Ints" $
      Z.define 1 `shouldBe` Z.z1
    it "Convert from negative Prelude Ints" $
      Z.define (-1) `shouldBe` Z.z_1
